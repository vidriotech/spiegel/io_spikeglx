// Copyright (c) 2017 Vidrio Technologies, All Rights Reserved
// Author: Nathan Clack <nathan@vidriotech.com>

#include <stddef.h>

#define export __declspec(dllexport)

#define QUOTE(name) #name
#define STR(macro) QUOTE(macro)

export const char * spikegl_reader_api_version() {
#ifdef GIT_TAG
    return STR(GIT_TAG) "-" STR(GIT_HASH);    
#else
    return "unknown";
#endif
}
