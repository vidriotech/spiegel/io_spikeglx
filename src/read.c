// Copyright (c)  Vidrio Technologies, All Rights Reserved
// Author: Nathan Clack <nathan@vidriotech.com>

#include "spikegl.h"
#include <windows.h>
#include <stdio.h>
#include <stddef.h>

#define export __declspec(dllexport)

#define DBG_(L,...) LOG_(L,__VA_ARGS__)
#define LOG_(L,...) if(L) (((const struct spikegl_logger*)L)->logger)((L),0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define ERR_(L,...) if(L) (((const struct spikegl_logger*)L)->logger)((L),1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define CHECK_(L,e) do{if(!(e)) {ERR_(L,"Error: Assertion failed -- %s\n",#e); goto Error;}}while(0)
#define CHECK_WIN32_(L,e) do{if(!(e)) {ERR_(L,"Error: %s\n\t%s\n",#e,win32_last_error_message()); goto Error;}}while(0)
#define CHECK_HANDLE_(L,e) do{if((e)==INVALID_HANDLE_VALUE) {ERR_(L,"Error: %s\n\t%s\n",#e,win32_last_error_message()); goto Error;}}while(0)

#define CHECK_PARSE(L,e) do{const char* reason=0; if(reason=(e)) {ERR_(L,"Error: Parse failed -- %s\n",reason); goto Error;}}while(0)
#define CHECK_FILE_(L,path,e) do{if((e)==INVALID_HANDLE_VALUE) {ERR_(L,"Error: %s\n\t%s\n\t%s\n",#e,win32_last_error_message(),path); goto Error;}}while(0)

#define LOG(...) LOG_(&self->ctx->logger,__VA_ARGS__)
#define ERR(...) ERR_(&self->ctx->logger,__VA_ARGS__)
#define CHECK(e) do{if(!(e)) {ERR("Error: Assertion failed -- %s\n",#e); goto Error;}}while(0)
#define CHECK_WIN32(e) do{if(!(e)) {ERR("Error: %s\n\t%s\n",#e,win32_last_error_message()); goto Error;}}while(0)
#define CHECK_HANDLE(e) do{if((e)==INVALID_HANDLE_VALUE) {ERR("Error: %s\n\t%s\n",#e,win32_last_error_message()); goto Error;}}while(0)

#define countof(e) (sizeof(e)/sizeof(*(e)))
#define containerof(P,T,M) ((T*)((char*)(P)-offsetof(T,M)))

static const char* win32_last_error_message() {
    static char buf[1024]={0};
    memset(buf,0,sizeof(buf));
    FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM,0,GetLastError(),0,buf,sizeof(buf),0);
    return buf;
}

//
//                  DATA TYPES
//					(some, not all)
//

/** Type id's used for parsing metadata. */
enum typeid {
    id_u32,
    id_u64,
};

/**
 * Represents data extracted from the filename
 */
struct file_name_data {
    enum file_kind {
        file_kind_bin,
        file_kind_meta
    } kind;    
    int trigger_id;
    char stream_name[256];
    char prefix[256];
};

/**
 * Metadata read from a .meta file
 */

struct meta {
    uint64_t first_sample;    // "firstSample" optional, default=0
    uint32_t nchan;           // "nSavedChans"
    uint64_t bytesof_data;    // "fileSizeBytes"
};

#include "events.c"

//
//                  MEMORY
//

/**
 * workspace describes a very simply memory allocator.
 * `p` points to the first byte of the next free segment.
 * `capacity` describes the remaining free bytes.
 */
struct workspace {
    uint8_t *p;
    size_t capacity;
};

/** 
 * Every successful allocation shifts `p` and computes the remaining capacity.
 * When the number of requested bytes exceeds capacity the allcoation fails.
 * 
 * Allocations are done in constant time.
 * There is no reclaimation of memory once allocated.
 * 
 * Some other simple possibilities would be
 *      to keep lists of free'd, used regions.
 *      allow freeing of the last allocated object (just tell workspace how many bytes to back up)
 *      allow temp storage allocated from the end of the heap
 *
 * @returns NULL on failure, otherwise a valid pointer.
 */
static void* workspace_alloc(struct workspace *w,size_t nbytes) {
    if(nbytes>w->capacity)
        return 0;
    w->capacity-=nbytes;
    void *p=w->p;
    w->p+=nbytes;
    return p;
}

//
//                  READER
//

struct index {
    struct response response;
    struct entry {
        uint64_t beg,end;
        int trigger_id;
    } *entries;
    int nentries;
    struct file_name_data file_name_data; ///< Example file_name_data for reconstructing filenames later.
    struct meta meta; ///< Metadata extracted from the last metafile.  Useful for querying common metadata entries.
    struct spikegl_roi bounds;
    const char* root_path; ///< points to reader->path
    HANDLE shape_ready; ///< Waitable event notifying when dataset bounds are known.
    enum spikegl_status shape_status;
};

struct reader {
    struct workspace workspace;
    struct request_queue requests;
    struct spikegl_reader_ctx *ctx;
    struct thread_pool thread_pool;

    char path[MAX_PATH];
    char stream[MAX_PATH]; // eg nidq or imec0.ap
    struct index index;
    int64_t refcount;
};

static struct reader *reader_alloc(const struct spikegl_reader_ctx *ctx) {
    struct workspace w={ctx->workspace,ctx->bytesof_workspace};
    struct reader *r;    
    CHECK_(&ctx->logger,r=(struct reader*)workspace_alloc(&w,sizeof(*r)));
    memset(r,0,sizeof(*r));
    r->workspace=w;
    r->ctx=(struct spikegl_reader_ctx *)ctx; // FIXME; sloppy const.  The context object should not be const at the api level
    r->index.shape_ready=CreateEvent(0,TRUE,FALSE,0);
    r->index.shape_status=spikegl_status_err;
    return r;
Error:
    LOG_(&ctx->logger,"Failed on a request for %llu bytes",(uint64_t)sizeof(*r));
    return 0;
}

static int is_opened(const struct reader* reader) {
    const enum response_status s=reader->index.response.status;
    return s!=response_uninitialized && s!=response_err;
}

export spikegl_status_t 
spikegl_reader_create(
    struct spikegl_reader *r,
    const struct spikegl_reader_ctx *ctx
) {    
    CHECK_(&ctx->logger,r->self=reader_alloc(ctx));  
    struct reader *self=(struct reader*)r->self;
    request_queue_init(&self->requests,&ctx->logger);
    return spikegl_status_ok;
Error:
    return spikegl_status_err;
}

static void destroy_(struct reader *self) {
    if(self->thread_pool.q) {
        // Thread pool is init'd on open.
        // An init'd threadpool will always have a reference to the queue.
        thread_pool_sig_stop(&self->thread_pool);
        // After waiting, no new  work items will be submitted.
        thread_pool_wait(&self->thread_pool);
    }
    {   // Wait for already executing items to complete
        struct response *r;
        while(r=outstanding_pop_tail(&self->requests)) 
            await(&self->requests,r);
    }
    CloseHandle(self->index.shape_ready);
}

export void 
spikegl_reader_destroy(struct spikegl_reader *r) {
    if(!r) return;
    struct reader *self=(struct reader*)r->self;
    if(!self) return;
    if(--self->refcount<=0)
        destroy_(self);
}

/**
 * walk through the file name from the right to the left
 * <name>_g<gate_id>_t<trigger_id>.<stream>.<ext>
 * 
 * <stream> might be imec<imec_id>.<ap_or_lf>
 *                  or nidq
 * 
 * At the moment this is written to extract the least 
 * information I need.  It should be straightforward to 
 * extend as necessary.
 *
 * @returns NULL on success, otherwise a non-NULL pointer to
 *          a string with the reason for the failure.
 */
static const char* parse_file_name(const char* filename, struct file_name_data *data) {
    struct {char *beg,*end;} trigger={0,0},stream={0,0};
    // There will be some fiddling with the filename string, but we'll
    // leave it the way we found it when we're done.
    char *c=(char*)filename+strlen(filename);
    // ext
    while(c-->filename && *c!='.');
    if(c==filename) return "Could not isolate the file extension.";
    if(strcmp(c,".meta")==0) {
        data->kind=file_kind_meta;
    } else if(strcmp(c,".meta")==0) {
        data->kind=file_kind_bin;
    }

    // stream    
    stream.end=c;
    {
        // find the first occurance of a '.' in the filename (from the left).
        // This will be stream.beg.
        for(c=(char*)filename;c<stream.end && *c!='.';++c);
        if(c==stream.end)
            return "Could not find the stream id.";
        stream.beg=c+1;
        c=stream.beg-1;
    }
    {
        size_t n=stream.end-stream.beg;
        // Check the stream name and store
        if( n<4 )
            return "The found stream id is too short to be valid, Must contain at least `nidq` or `imec`.";
    
        memset(data->stream_name,0,sizeof(data->stream_name));
        if( n>=sizeof(data->stream_name) )
            return "The found stream id is too long to store.";
        memcpy(data->stream_name,stream.beg,stream.end-stream.beg);
    }    
    
    // trigger_id
    trigger.end=c;
    while(c-->filename && *c!='_');
    if(c==filename) return 0;
    trigger.beg=c+1;

    if(!(trigger.beg && trigger.beg[0]=='t'))
        return "Could not find the trigger_id";
    char *stop;
    *trigger.end='\0';
    data->trigger_id=strtol(trigger.beg+1,&stop,10);	
    // restore the end character since we want the
    // string to look const to the outside world.
    *trigger.end='.';
    if(stop!=trigger.end)
        return "Could not parse trigger_id as an integer.";

    // prefix
    *(char*)c='\0';
    strcpy_s(data->prefix,sizeof(data->prefix),filename);
    *(char*)c='_'; // restore
    return 0;
}

static void construct_file_name(char* dst,size_t nbytes,const struct file_name_data *data) {
    sprintf_s(dst,nbytes,"%s_t%u.%s.%s"
              ,data->prefix
              ,data->trigger_id
              ,data->stream_name
              ,(data->kind==file_kind_meta)?"meta":"bin"
    );
}

/**
 * djb2: small,simple hash function by Dan Bernstein
 * if end is set to 0, it will just treat str as NULL terminated
 */
static uint32_t djb2(const char *str_,const char *end_) {
    uint32_t hash=5381;
    const uint8_t *str=(const uint8_t*)str_;
    const uint8_t *end=(const uint8_t*)end_;
    int c;
    while(str!=end && (c=*str++)) hash=hash*33+c;
    return hash;
}

/**
 * Parse the metadata file.
 * Expect the input to be text buffer with the following grammer:
 * 
 *	  line ::= key '=' [value] end-of-line line
 *	         | key '=' [value] end-of-buffer
 *	  end-of-line   ::= '\r\n' | '\n'
 *	  end-of-buffer ::= '\0'
 *	  key   ::= (anything without the above tokens)
 *	  value ::= (anything that parses according to the handlers in the code)
 *	  
 * Approach here is to search out specific keys rather than parse everything.
 */
static int parse_meta(struct meta *meta,const char* input,size_t bytesof_input) {
    const char *end=input+bytesof_input;
    struct {
        enum typeid type;	
        void *dst;
        const uint32_t key;
    } handlers[] = {
        {id_u64,&meta->first_sample,djb2("firstSample",0)},
        {id_u32,&meta->nchan       ,djb2("nSavedChans",0)},
        {id_u64,&meta->bytesof_data,djb2("fileSizeBytes",0)},
    };
    for(const char* c=input;c<end;c++) {
        struct {const char*beg,*end;} key={0,0},value={0,0};
        key.beg=c;
        while(c<end && *c!='=') c++;
        key.end=c;
        value.beg=c+1;
        while(c<end && *c!='\n') c++;
        value.end=c-(c[-1]=='\r');
        
        const uint32_t hash=djb2(key.beg,key.end);
        char *stop;
        for(int i=0;i<countof(handlers);++i) if(hash==handlers[i].key) {			
            switch(handlers[i].type) {
                case id_u32: 
                    *(uint32_t*)handlers[i].dst=strtol(value.beg,&stop,10);
                    if(stop!=value.end)
                        return 0;
                    break;
                case id_u64: 
                    *(uint64_t*)handlers[i].dst=strtoull(value.beg,&stop,10);
                    if(stop!=value.end)
                        return 0;
                    break;
                default:
                    return 0; // need to add a case for the type
            }			
        }
    }	
    return 1;
}

static int parse_meta_from_file(struct meta *meta,const char* path,const struct spikegl_logger* logger) {
    HANDLE h;
    char buf[1024]={0};
    DWORD nread;
    CHECK_HANDLE_(logger,h=CreateFile(path,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0));			
    CHECK_(logger,ReadFile(h,buf,sizeof(buf),&nread,0));
    CloseHandle(h);
    CHECK_(logger,parse_meta(meta,buf,sizeof(buf)));
    return 1;
Error:
    LOG_(logger,"Failed to open %s",path);
    return 0;
}

struct build_index_in {
    struct request request;
    struct workspace *workspace;
    char* path;
    const char* stream;
};

static enum response_status 
build_index_(
    struct request_queue* q,
    struct response *response,
    const struct request *request
) {
    struct index *index=containerof(response,struct index,response);
    // declare this as const, but it's not really
    // - we will update the path
    const struct build_index_in *args=containerof(request,const struct build_index_in,request);
    enum response_status status=response_ok;
    struct file_name_data parse_mx,parse_mn={0};
    struct meta meta_mn={0},meta_mx={0};

    index->root_path=args->path;

    HANDLE h=INVALID_HANDLE_VALUE;
    const struct spikegl_logger *L=q->logger;
    struct workspace *w=args->workspace;

    {            
        char query[MAX_PATH]={0};
        CHECK_(L,!strcat_s(query,sizeof(query),args->path));

        // Canonicalize and validate path
        {        
            HANDLE d;
            size_t nbytes;
            CHECK_FILE_(L,query,d=CreateFileA(query,
                GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,FILE_FLAG_BACKUP_SEMANTICS,0));
            CHECK_WIN32_(L,nbytes=GetFinalPathNameByHandle(d,(char*)query,MAX_PATH,FILE_NAME_NORMALIZED));
            CloseHandle(d);
        }
        LOG_(L,"Stream: %s\n",args->stream);
        CHECK_(L,!strcat_s(query,sizeof(query),"\\*."));
        CHECK_(L,!strcat_s(query,sizeof(query),args->stream));
        CHECK_(L,!strcat_s(query,sizeof(query),".meta"));
        LOG_(L,"Query: %s\n",query);

        // Find the min/max meta file (min/max by trial id)
        {
            int nfiles=1;
            WIN32_FIND_DATA data;
            struct file_name_data parse;
            CHECK_FILE_(L,query,h=FindFirstFile(query,&data));
            CHECK_PARSE(L,parse_file_name(data.cFileName,&parse));
            int mx=parse.trigger_id;
            int mn=parse.trigger_id;
            while(FindNextFile(h,&data)) {
                CHECK_PARSE(L,parse_file_name(data.cFileName,&parse));
                mx=(parse.trigger_id>mx)?parse.trigger_id:mx;
                mn=(parse.trigger_id<mn)?parse.trigger_id:mn;
                nfiles++;
            }
            FindClose(h);
            CHECK_(L,mx>=0);            
            index->nentries=nfiles;
            CHECK_(L,index->entries=(struct entry*)workspace_alloc(w,sizeof(*index->entries)*nfiles));
            parse_mx=parse;
            parse_mx.trigger_id=mx;
            parse_mn=parse;
            parse_mn.trigger_id=mn;
        }
        {
            // Parse the first meta file.
            {
                char path[1024]={0};
                char filename[256]={0};
                construct_file_name(filename,sizeof(filename),&parse_mn);
                CHECK_(L,!strcat_s(path,sizeof(path),args->path));
                CHECK_(L,!strcat_s(path,sizeof(path),"\\"));
                CHECK_(L,!strcat_s(path,sizeof(path),filename));
                CHECK_FILE_(L,path,h=CreateFile(path,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0));
            }
            {
                char buf[1024]={0};
                DWORD nread;
                CHECK_(L,ReadFile(h,buf,sizeof(buf),&nread,0));
                CloseHandle(h);
                CHECK_(L,parse_meta(&meta_mn,buf,sizeof(buf)));
            }
        }
        {
            // Parse the last meta file.
            {
                char path[1024]={0};
                char filename[256]={0};
                construct_file_name(filename,sizeof(filename),&parse_mx);
                CHECK_(L,!strcat_s(path,sizeof(path),args->path));
                CHECK_(L,!strcat_s(path,sizeof(path),"\\"));
                CHECK_(L,!strcat_s(path,sizeof(path),filename));
                CHECK_FILE_(L,path,h=CreateFile(path,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0));
            }
            {
                char buf[1024]={0};
                DWORD nread;
                CHECK_(L,ReadFile(h,buf,sizeof(buf),&nread,0));
                CloseHandle(h);
                CHECK_(L,parse_meta(&meta_mx,buf,sizeof(buf)));
            }
        }
        // Compute what we need to resolve read requests later.
        index->file_name_data=parse_mx;
        index->meta=meta_mx;
        {
            const uint64_t beg=meta_mn.first_sample,
                        end=meta_mx.first_sample+meta_mx.bytesof_data/2/meta_mx.nchan; // samples are 2-bytes wide
            index->bounds.offset.time=beg;
            index->bounds.offset.channels=0;
            index->bounds.shape.time=end-beg;
            index->bounds.shape.channels=meta_mx.nchan;
        }     
        index->shape_status=spikegl_status_ok;   
        SetEvent(index->shape_ready); // Notify bounds are ready
        if(q->is_running) {
            // Build table of entries
            WIN32_FIND_DATA data;
            struct file_name_data parse;            
            h=FindFirstFile(query,&data);
            int ifile=0;
            do {		
                //DBG_(L,"     Entry: %5d %s",ifile,data.cFileName);
                struct meta meta={0};
                CHECK_PARSE(L,parse_file_name(data.cFileName,&parse));
                memset(query,0,sizeof(query)); // confusingly, reuse query buffer for our file path
                strcat_s(query,sizeof(query),args->path);
                strcat_s(query,sizeof(query),"\\");
                strcat_s(query,sizeof(query),data.cFileName);
                CHECK_(L,parse_meta_from_file(&meta,query,L));
                index->entries[ifile].beg=meta.first_sample;
                index->entries[ifile].end=meta.first_sample+meta.bytesof_data/2/meta.nchan;
                index->entries[ifile].trigger_id=parse.trigger_id;
                ifile++;
            } while(FindNextFile(h,&data) && q->is_running);
            FindClose(h);
            CHECK_(L,ifile==index->nentries);
        }
    }
Finalize:
    free((void*)args);
    return status;
Error:
    LOG_(L,"Build index failed %sfor %s",    
        q->is_running?"":"(Aborted) ",
        args->path);
    SetEvent(index->shape_ready); // Notify so shape query doesn't get stuck
    status=response_err;
    index->shape_status=spikegl_status_err;
    SetEvent(index->shape_ready); // Notify so any pending waits abort
    goto Finalize;
}

static void build_index_async(struct reader* self,struct index *index) {
    struct build_index_in *args=(struct build_index_in*)malloc(sizeof(*args));
    request_init(&args->request,&index->response,build_index_);
    args->workspace=&self->workspace;
    args->path=self->path;
    args->stream=self->stream;
    request_submit(&self->requests,&args->request);
}

export spikegl_status_t 
spikegl_reader_open(struct spikegl_reader *r,const char* path, const char* stream) {
    CHECK_(0,r);	
    struct reader *self=(struct reader*)r->self;	
    CHECK_(0,self); // before you wreck self
    CHECK(path!=NULL);
    ++self->refcount;
    ResetEvent(self->index.shape_ready);
    self->index.shape_status=spikegl_status_err;
    thread_pool_start(&self->thread_pool,&self->requests);
    strcpy_s(self->path,sizeof(self->path),path);
    if(stream)
        strcpy_s(self->stream,sizeof(self->stream),stream);
    else
        // use a default value if stream is NULL
        strcpy_s(self->stream,sizeof(self->stream),"nidq");
    build_index_async(self,&self->index);

    return spikegl_status_ok;
Error:
    return spikegl_status_err;
}

export spikegl_status_t 
spikegl_reader_shape(const struct spikegl_reader *r,struct spikegl_roi *roi) {
    struct reader *self=0;
    CHECK_(0,r);	
    self=(struct reader*)r->self;	
    CHECK_(0,self); // before you wreck self
    WaitForSingleObject(self->index.shape_ready,INFINITE);
    if(roi) *roi=self->index.bounds;
    return self->index.shape_status;
Error:
    return spikegl_status_err;
}

export size_t 
spikegl_reader_nbytes(const struct spikegl_roi *roi) {
    return roi->shape.channels*roi->shape.time*2; // 2 bytes per sample
}

export struct spikegl_dims 
spikegl_reader_strides(const struct spikegl_roi *roi) {
    return (struct spikegl_dims){
        .channels=1,
        .time=roi->shape.channels
    };
}

static void intersect(struct spikegl_roi *out,const struct spikegl_roi *a,const struct spikegl_roi *b) {
    // this looks gross.  Usually prefer representing roi's by storing min, max corners
    // to simplify this intersection calculation.  Another way to clean it up, might be
    // to cast the roi's to int64[4] and operate on indexes instead of struct fields.
    const struct spikegl_dims
        afar={.time    =a->offset.time    +a->shape.time,
              .channels=a->offset.channels+a->shape.channels},
        bfar={.time    =b->offset.time    +b->shape.time,
              .channels=b->offset.channels+b->shape.channels},
        mn  ={.time    =max(a->offset.time    ,b->offset.time),
              .channels=max(a->offset.channels,b->offset.channels)},
        mx  ={.time    =min(afar.time    ,bfar.time),
              .channels=min(afar.channels,bfar.channels)};
    *out=(struct spikegl_roi){.offset=mn,
        .shape={.time    =max(mx.time    -mn.time,0),
                .channels=max(mx.channels-mn.channels,0)}};
}

static int is_empty(const struct spikegl_roi *a) {
    return a->shape.time<=0 || a->shape.channels<=0;
}

//
//                  READ ROI FROM FILE
//

/**
 * Request to read a specific roi from a file into a pre-allocated buffer
 * 
 * Passes the entry_id to the response object so we know which entry the
 * file came from.  Otherwise the entry id is unused.
 */
struct read_roi_from_file_in {
    struct request request;
    unsigned entry_id; ///< index into the entries array (see reader index)

    struct {
        uint8_t *ptr;
        size_t stride; // in bytes
    } mem;

    struct {        
        char path[MAX_PATH];
        uint8_t* offset; 
        size_t stride;  // in bytes
    } file;

    int64_t w,h; // w must be in bytes. h is the number of lines to read
};

struct read_roi_from_file_out {
    struct response response;
    unsigned entry_id;
};

static enum response_status
read_roi_from_file( 
    struct request_queue *q,
    struct response *response,
    const struct request *request
) {
    const struct read_roi_from_file_in *in=containerof(request,const struct read_roi_from_file_in,request);
    struct read_roi_from_file_out *out=containerof(response,struct read_roi_from_file_out,response);
    const struct spikegl_logger *L=q->logger;
    enum response_status ecode=response_ok;
    HANDLE h;
    out->entry_id=in->entry_id;
    HANDLE events[64]={0};
    OVERLAPPED o[64]={0};
    uint8_t *buf[64]={0};
    int len[64]={0};
    for(int i=0;i<countof(events);++i)
        events[i]=o[i].hEvent=CreateEvent(0,TRUE,FALSE,0);
        
    DBG_(L,"Read: %s",in->file.path);
    CHECK_HANDLE_(L,h=CreateFileA(in->file.path,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED//|FILE_FLAG_RANDOM_ACCESS
        ,0));
    const DWORD w=(DWORD)in->w;
    const struct {uint8_t *beg,*end;} range={
        .beg=in->file.offset,
        .end=in->file.offset+(in->h-1)*in->file.stride+in->w
    };
    int j=0; // event index    
    for(uint8_t *cur=range.beg;cur<range.end;) {
        if(!buf[j])
            CHECK_(L,buf[j]=malloc(1<<20));

        o[j].Pointer=cur;
        DWORD nbytes=(DWORD)(min(1LL<<20,(int64_t)(range.end-cur)));
        ReadFile(h,buf[j],nbytes,0,o+j);
        len[j]=nbytes;
        cur+=nbytes;

        ++j;
        if(j==countof(events)) {
            WaitForMultipleObjects(countof(events),events,TRUE,INFINITE);
			// Now that the buffers are filled, do strided copy to the
			// output.
			for(int k=0;k<j;++k) {
				// 1. find the origin of the block
				//      wrt the output offset   
				uint8_t *src=buf[k];
				const int64_t src_offset=((int64_t)o[k].Pointer)-(int64_t)range.beg;
				const int64_t x0=(src_offset)%in->file.stride;
				const int64_t y0=(src_offset)/in->file.stride;
				const int64_t dst_offset=y0*w;
				// 2. copy the ragged first line
				if(x0<w) 
					memcpy(in->mem.ptr+dst_offset+x0,src,min(len[k],w-x0));
				// 3. copy the non-ragged in->h-2 lines
				src+=in->file.stride-x0;
				len[k]-=(int)(in->file.stride-x0);
				int64_t iline=1;
				while(len[k]>(int)w) {
					memcpy(in->mem.ptr+dst_offset+iline*w,src,w);
					src+=in->file.stride;
					len[k]-=(int)(in->file.stride);
					iline++;
				}
				// 4. copy the last ragged line.
				if(len[k]>0)
					memcpy(in->mem.ptr+dst_offset+iline*w,src,len[k]);

				ResetEvent(events[k]);
				len[k]=0;
            }
            j=0;
        }
    }

    if(j) {
        WaitForMultipleObjects(j,events,TRUE,INFINITE);
        // Now that the buffers are filled, do strided copy to the
        // output.
        for(int k=0;k<j;++k) {
			// 1. find the origin of the block
			//      wrt the output offset   
			uint8_t *src=buf[k];
			const int64_t src_offset=((int64_t)o[k].Pointer)-(int64_t)range.beg;
			const int64_t x0=(src_offset)%in->file.stride;
			const int64_t y0=(src_offset)/in->file.stride;
			const int64_t dst_offset=y0*w;
			// 2. copy the ragged first line
			if(x0<w) 
				memcpy(in->mem.ptr+dst_offset+x0,src,min(len[k],w-x0));
			// 3. copy the non-ragged in->h-2 lines
			src+=in->file.stride-x0;
			len[k]-=(int)(in->file.stride-x0);
			int64_t iline=1;
			while(len[k]>(int)w) {
				memcpy(in->mem.ptr+dst_offset+iline*w,src,w);
				src+=in->file.stride;
				len[k]-=(int)(in->file.stride);
				iline++;
			}
			// 4. copy the last ragged line.
			if(len[k]>0)
				memcpy(in->mem.ptr+dst_offset+iline*w,src,len[k]);

			ResetEvent(events[k]);
			len[k]=0;
        }
    }
Finalize:
    
    CloseHandle(h);
    for(int i=0;i<countof(events);++i) {
        CloseHandle(events[i]);
        free(buf[i]);
    }
    return ecode;
Error:
    ecode=response_err;
    goto Finalize;
}

//
//                  READ ROI
//

struct read_roi_in {
    struct request request;
    struct spikegl_roi roi;
    void *buf;
    size_t bytesof_buf;
    const struct index *index;
};

struct read_roi_out {
    struct response response;
    struct reader *reader;
    struct spikegl_roi roi;
    void *buf;
    size_t bytesof_buf;
};

static void 
entry_to_roi(struct spikegl_roi* dst, const struct entry* e, int64_t nchan) {
    *dst = (struct spikegl_roi){
        .offset={.time=e->beg,       .channels=0},
        .shape ={.time=e->end-e->beg,.channels=nchan}
    };
}

/**
 * Returns the byte offset between the origin of the outer and inner rois.
 * The "outer" roi determines the stride.
 */
static int64_t
offset_rois(const struct spikegl_roi *outer,const struct spikegl_roi *inner) {
    size_t stride=outer->shape.channels;
    return 2*(
        stride*(inner->offset.time-outer->offset.time)
        +(inner->offset.channels-outer->offset.channels));
}

/**
 * @returns 0 if there is no data to load for the entry, otherwise 1
 */
static int 
make_read_roi_from_file_request(
    struct read_roi_from_file_in *self,
    const struct read_roi_in* src,
    const struct entry* e,
    const char* root_path
) {
    struct file_name_data fnd=src->index->file_name_data;
    fnd.trigger_id=e->trigger_id;
    fnd.kind=file_kind_bin;
    char fname[MAX_PATH];
    construct_file_name(fname, sizeof(self->file.path),&fnd);
    memset(self->file.path,0,sizeof(self->file.path));
    strcat_s(self->file.path,sizeof(self->file.path),root_path);
    strcat_s(self->file.path,sizeof(self->file.path),"\\");
    strcat_s(self->file.path,sizeof(self->file.path),fname);

    // compute intersection
    struct spikegl_roi entry_roi,roi;
    entry_to_roi(&entry_roi, e, src->index->bounds.shape.channels);
    intersect(&roi,&src->roi,&entry_roi);
    if(is_empty(&roi))
        return 0;

    // fill in the info for the copy
    self->w=roi.shape.channels*2; // 2 bytes per sample
    self->h=roi.shape.time;
    self->mem.stride=src->roi.shape.channels*2;   // 2 bytes per sample
    self->file.stride=entry_roi.shape.channels*2; // 2 bytes per sample
    self->mem.ptr=(uint8_t*)src->buf+offset_rois(&src->roi,&roi);
    self->file.offset=(uint8_t*)offset_rois(&entry_roi,&roi);
    return 1;
}


/**
 * Given an roi, need to know how to fill it up.
 * 1. Zero the buf (at least fill in with some background value)
 * 2. Iterate over rois for indexed files, if there's an intersection:
 *     a. do stuff
 */					
static enum response_status read_roi(struct request_queue *q, struct response* response,const struct request *request) {
    enum response_status ecode=response_ok;
    struct read_roi_out *out=containerof(response,struct read_roi_out,response);
    struct read_roi_in *in=containerof(request,struct read_roi_in,request);
    const struct spikegl_logger *L=q->logger;
    memset(in->buf,0,in->bytesof_buf);
    CHECK_(L,await(q,(struct response*)(&in->index->response))==response_ok); // make sure index is build first
    const struct entry* const entries=in->index->entries;
    const struct entry *const end=in->index->entries+in->index->nentries;	
    struct read_roi_from_file_in reqs[64]={0};
    struct read_roi_from_file_out rsps[64]={0};
    int ireq=0;
    for(int i=0;i<countof(reqs);++i) 
        request_init(&reqs[i].request,&rsps[i].response,read_roi_from_file);
    unsigned ientry=0;
    for(const struct entry *e=entries;e<end;++e,++ientry) {        
        if(make_read_roi_from_file_request(reqs+ireq,in,e,in->index->root_path)) {
            reqs[ireq].entry_id=ientry;
            request_submit(q,&reqs[ireq].request);
            ireq++;
        }
        // Do we need to burn down requests?
        if(ireq==countof(reqs)) {
            for(unsigned i=0;i<countof(reqs);++i) {
                CHECK_(L,await(q,&rsps[i].response)==response_ok);
            }
            ireq=0;
        }
    }
    // wait on and handle any outstanding requests
    while(ireq-->0) {
        CHECK_(L,await(q,&rsps[ireq].response)==response_ok);
    }

Finalize:
    // Now, that we're done with the request.  Need to free.
    free(in);
    return ecode;
Error:
    ecode=response_err;
    goto Finalize;
}

/**
 * @returns spikegl_status_ok on success, otherwise an error code.
 *
 * @param future       [out] Use this with 'spikegl_await' to wait until the read is completed.
 * @param r            [in]  An opened reader object.
 * @param roi          [in]  The region of interest to load.
 * @param buf          [in]  A buffer allocated by the caller for holding the loaded data.
 *                           Use 'spikegl_reader_nbytes()' to calculate the minimum required
 *                           size of this buffer in bytes.
 * @param bytesof_buf  [in]  The size, in bytes, of the buffer pointed to by 'buf'.
 */
export spikegl_status_t
spikegl_reader_read_async(
    struct spikegl_future *future,
    struct spikegl_reader *r,
    const struct spikegl_roi *roi,
    uint16_t *buf,
    size_t bytesof_buf
) {	
    CHECK_(0,r);	
    struct reader *self=(struct reader*)r->self;
    CHECK_(0,self); // before you wreck self
    CHECK(is_opened(self));
    CHECK(bytesof_buf>=spikegl_reader_nbytes(roi));
    CHECK(sizeof(*future)>=sizeof(struct read_roi_out));    // This could/should be a static compile-time check    
    struct read_roi_out* out=(struct read_roi_out*)future; // Cast to private type
    ++self->refcount; // future holds a reference to the reader so incref
    out->reader=self;
    out->buf=buf;
    out->bytesof_buf=bytesof_buf;
    out->roi=*roi;

    struct read_roi_in *in;
    CHECK(in=(struct read_roi_in*)malloc(sizeof(*in)));
    memset(in,0,sizeof(*in));
    memcpy(&in->roi,roi,sizeof(in->roi));
    in->buf=buf;
    in->bytesof_buf=bytesof_buf;
    in->index=&self->index;
    request_init(&in->request,&out->response,read_roi);
    request_submit(&self->requests,&in->request);
    return spikegl_status_ok;
Error:
    return spikegl_status_err;
}

/**
 * Blocks until a read request is completed.  The region-of-interest,
 * output buffer, and buffer size used to initiate the request are 
 * returned through the function arguments.
 * 
 * spikegl_future objects carry a reference to the reader used to generate them.
 * The lifetime of the reader must exceed the lifetime of the future.
 * 
 * @returns spikegl_status_ok on success, otherwise an error code.
 *
 * @param f      [in]  A spikegl_future obtained from 'spikegl_reader_read_async()'.
 *                     Tracks when a request is done.
 * @param roi    [out] The region of interest used to initiate the load request.
 * @param buf    [out] A pointer to the buffer containing loaded data.
 * @param bytesof_buf [out] size of the output data buffer in bytes.
 * @param nbytes [out] size of the data buffer in bytes.
 */
export spikegl_status_t 
spikegl_await(
    struct spikegl_future *f,
    struct spikegl_roi *roi,
    void **buf,
    size_t *bytesof_buf
) {
    CHECK_(0,f);	
    struct read_roi_out* read=(struct read_roi_out*)f; // Cast to private type
    struct reader *self=read->reader;
    CHECK_(0,self); // before you wreck self	
    CHECK(await(&self->requests,&read->response)==response_ok);
    if(roi)         *roi        =read->roi;
    if(buf)         *buf        =read->buf;
    if(bytesof_buf) *bytesof_buf=read->bytesof_buf;
    struct spikegl_reader rr={.self=read->reader};
    spikegl_reader_destroy(&rr); // dec ref and maybe destroy
    return spikegl_status_ok;
Error:
    return spikegl_status_err;
}

export spikegl_status_t 
spikegl_reader_read(
    struct spikegl_reader *r,
    const struct spikegl_roi *roi,
    uint16_t *buf,
    size_t bytesof_buf
) {
    struct spikegl_future f;
    struct reader *self=(struct reader*)r->self;

    CHECK(sizeof(struct spikegl_future)>=sizeof(struct read_roi_out)); //  TODO: remove me.

    // FIXME: check return values
    CHECK(spikegl_reader_read_async(&f,r,roi,buf,bytesof_buf)==spikegl_status_ok);
    CHECK(spikegl_await(&f,0,0,0)==spikegl_status_ok);

    return spikegl_status_ok;
Error:
    return spikegl_status_err;
}

export
unsigned spikegl_reader_ntrials(struct spikegl_reader *r) {
    CHECK_(0,r);	
    struct reader *self=(struct reader*)r->self;
    CHECK_(0,self); // before you wreck self
    CHECK(await(&self->requests,&self->index.response)==response_ok);
    return self->index.nentries;
Error:
    return 0;
}

export
spikegl_status_t spikegl_reader_get_roi(
    struct spikegl_reader *r,
    struct spikegl_roi *roi,
    unsigned itrial
) {
    CHECK_(0,r);	
    struct reader *self=(struct reader*)r->self;
    CHECK_(0,self); // before you wreck self
    CHECK(roi);
    CHECK(await(&self->requests,&self->index.response)==response_ok);
    const struct entry* es=self->index.entries;
    for(int i=0;i<self->index.nentries;++i)
        if(es[i].trigger_id==itrial) {
            entry_to_roi(roi,es+i,self->index.bounds.shape.channels);
            return spikegl_status_ok;
        }
Error:
    return spikegl_status_err;
}

//                  DEFAULT CONTEXT
//                  Define an FFI friendly version of the context that 
//                  logs to a resizable string buffer.
//

struct reader_ctx_v1 {
    struct spikegl_reader_ctx ctx;
    char *log;
    size_t len,capacity;
    SRWLOCK lock;
};

/** request log buffer have enough room to append a string nbytes long.
 */
static void maybe_resize(struct reader_ctx_v1 *self,size_t nbytes) {
    const size_t required=self->len+nbytes;
    if(required>=self->capacity) { // need to realloc
        self->capacity=(size_t)(1.2f*required+64);
        self->log=realloc(self->log,self->capacity);
        memset(self->log+self->len,0,self->capacity-self->len);
    }
}

static void default_ctx_logger(const struct spikegl_logger *self,int is_error,const char *file,int line,const char* function,const char *fmt,...) {
    struct reader_ctx_v1 *ctx=containerof(self,struct reader_ctx_v1,ctx.logger);
    AcquireSRWLockExclusive(&ctx->lock);
    
    char buf1[1024]={0},buf2[1024]={0};
    va_list ap;
    va_start(ap,fmt);
    vsprintf_s(buf1,sizeof(buf1),fmt,ap);
    va_end(ap);
#if 1
    sprintf_s(buf2,sizeof(buf2),"%s(%d): %s()\n\t - %s\n",file,line,function,buf1);
#else
    sprintf_s(buf2,sizeof(buf2),"%s\n",buf1);
#endif
    
    const size_t n=strlen(buf2);
    maybe_resize(ctx,n);
    if(ctx->log) {
        strcpy_s(ctx->log+ctx->len,ctx->capacity-ctx->len,buf2);
        ctx->len+=n;
    }
    // OutputDebugStringA(buf2);
	// puts(buf2);
    ReleaseSRWLockExclusive(&ctx->lock);
}

static
spikegl_status_t reader_ctx_v1_create(
    struct reader_ctx_v1* self,
    size_t bytesof_workspace
) {
    memset(self,0,sizeof(*self));
    self->ctx.workspace=malloc(bytesof_workspace);
    self->ctx.bytesof_workspace=bytesof_workspace;
    self->ctx.logger.logger=default_ctx_logger;
    if(self->ctx.workspace)
        return spikegl_status_ok;
    return spikegl_status_err;
}

static
void reader_ctx_v1_destroy(
    struct reader_ctx_v1* self
) {
    if(self && self->ctx.workspace) {
        // wait for the lock just in case someone was still hanging around
        AcquireSRWLockExclusive(&self->lock);
        free(self->ctx.workspace);        
        if(self->log) {            
            free(self->log);
        }            
        self->log=0;
        self->ctx.logger.logger=0;
        ReleaseSRWLockExclusive(&self->lock);
    }
}

export struct spikegl_reader_ctx* 
spikegl_default_context_create(
    size_t bytesof_workspace
) {
    struct reader_ctx_v1 *self=0;
    CHECK_(0,self=(struct reader_ctx_v1*)malloc(sizeof(*self)));
    CHECK_(0,reader_ctx_v1_create(self,bytesof_workspace)==spikegl_status_ok);
    return &self->ctx; 
Error:
    free(self);
    return 0;
}

export void 
spikegl_default_context_destroy(
    struct spikegl_reader_ctx *self_
) {
    struct reader_ctx_v1 *self=containerof(self_,struct reader_ctx_v1,ctx);
    reader_ctx_v1_destroy(self);
	free(self);
}

export const char* 
spikegl_default_context_log(struct spikegl_reader_ctx* self_,size_t* nbytes) {
    struct reader_ctx_v1 *self=containerof(self_,struct reader_ctx_v1,ctx);
    if(nbytes) *nbytes=self->len;
    return self->log;
}



/* NOTES

(1) Parallelizing the read

 - the roi will break down into regions to be read from a sequence of files.
   The operations on each file can be parallelized.
   - Just dispatch a bunch of overlapped reads
   - WFMO lets us wait on 64 things at a time?

(2) Regarding logging in a multithreaded environment

I ran into a weird issue where one of the printfs (usually a vsnprintf_s) would
segfault after the main thread exited.  Inside of the vsnprintf_s, it was 
calling _free_dbg which relies on a global CriticalSection associated with the 
c runtime.  Once the c runtime cleans itself up after the main thread exits, 
that CriticalSection is gone, and there's a segfault.

Having a problem with the c runtime suddenly drop off the face of the earth 
while other threads are running seems like a problem.  Maybe that's why they 
sugget using the beginthread_ api when using the c runtime. For now this is
solved, by just keeping track of the logger messages waiting at exit. This is 
also nice because I can ensure everything gets logged (as opposed to truncating 
the message output when the main thread exits).

*/