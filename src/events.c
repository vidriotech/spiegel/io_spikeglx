// Copyright (c) 2017 Vidrio Technologies, All Rights Reserved
// Author: Nathan Clack <nathan@vidriotech.com>

/**
 * @file Event system
 * 
 * This file is designed to be included into other c files.
 * All the function declarations are static.
 * The API defined here is intended to be private.
 * 
 * Eventually, I might delete this file and copy-and-paste it where it needs
 * to go.
 * 
 * The file including this one should include the relevant headers etc.
 */

#include <windows.h>
#include "spikegl.h"

#ifndef containerof
#define containerof(P,T,M) ((T*)((char*)(P)-offsetof(T,M)))
#endif

//
//                  REQUESTS
//                  A request queue is used to ensure all IO is non-blocking.
//

enum response_status {    
    response_uninitialized=0,
    response_waiting,
    response_ok,
    response_err, // a request failed
};

struct request {
    enum response_status (*callback)(
        struct request_queue*,
        struct response*, 
        const struct request*);
    struct request *next,*prev;
    struct response *response;
};

// This encapsulates a future
struct response {
    enum response_status status;
    struct response *next, *prev;
    HANDLE done;
};

struct request_queue {
    struct request_dlist {
        struct request *queue;
        struct request sentinal;
    } todo;
    struct response_dlist {
        struct response *queue;
        struct response sentinal;
    } outstanding;
    volatile int is_running; ///< gets set to 0 when thread pool exits
    HANDLE work_available;
    SRWLOCK lock;
    enum response_status last_ecode;    
    const struct spikegl_logger *logger;
};

static void request_dlist_init(struct request_dlist* self) {
    self->sentinal=(struct request){
        .callback=0,
        .next=&self->sentinal,
        .prev=&self->sentinal,
        .response=0};
    self->queue=&self->sentinal;
}

static void response_dlist_init(struct response_dlist* self) {
    self->sentinal=(struct response){
        .next=&self->sentinal,
        .prev=&self->sentinal,
        .done=INVALID_HANDLE_VALUE};
    self->queue=&self->sentinal;
}

static void request_queue_init(struct request_queue* self,const struct spikegl_logger *logger) {	
    memset(self,0,sizeof(*self));
    request_dlist_init(&self->todo);
    response_dlist_init(&self->outstanding);
    self->work_available=CreateEvent(0,TRUE,FALSE,0);
    self->last_ecode=response_ok;
    self->logger=logger;
    self->is_running=1;
}

static void request_queue_destroy(struct request_queue* self) {
    CloseHandle(self->work_available);
}

static void request_queue_wait_for_work(struct request_queue* self) {
    WaitForSingleObject(self->work_available,INFINITE);
}

/**
 * Insert a request into a request list after 'cur'
 */
static void insert_request(struct request* cur,struct request *r) {
    r->next=cur->next;
    r->prev=cur;
    cur->next->prev=r;
    cur->next=r;
}

/** Removes an item from the current position of the request list.
 *  @returns NULL if the list is empty, otherwise returns the removed item.
 */
static struct request* remove_request(struct request_dlist* dlist) {    
    struct request *cur=dlist->queue;
    if(cur->prev==&dlist->sentinal)
        return 0;
    struct request * item=cur->prev;
    item->prev->next=cur;
    cur->prev=item->prev;
    item->next=item->prev=item;
    return item;
}

/**
 * Puts a new request on the queue.  The queue takes ownership.
 * Ultimately the request callback is responsible for retiring/freeing 
 * the request.
 */
static void todo_push_head(struct request_queue* self,struct request *r) {
    AcquireSRWLockExclusive(&self->lock);
    insert_request(self->todo.queue,r);
    ReleaseSRWLockExclusive(&self->lock);
    SetEvent(self->work_available);
}

/** 
 * @returns 0 when empty, otherwise the oldest request on the queue.
 */
static struct request* todo_pop_tail(struct request_queue* self) {
    AcquireSRWLockExclusive(&self->lock);
    struct request *item=remove_request(&self->todo);
    if(!item) ResetEvent(self->work_available);
    ReleaseSRWLockExclusive(&self->lock);		
    return item;
}

/**
 * Insert a response into a response list after 'cur'
 */
static void insert_response(struct response* cur,struct response *r) {
    r->next=cur->next;
    r->prev=cur;
    cur->next->prev=r;
    cur->next=r;
}

/** Removes an item from the response list.
 *  @returns the removed item, or NULL if the list is empty
 */
static struct response* remove_response_item(struct response_dlist* dlist,struct response *item) {        
    if(item==&dlist->sentinal)
        return 0;
    item->next->prev=item->prev;
    item->prev->next=item->next;
    item->next=item;
    item->prev=item;
    return item;
}

/** Removes an item from the current position of the response list.
 *  @returns NULL if the list is empty, otherwise returns the removed item.
 */
static struct response* remove_response(struct response_dlist* dlist) {
    // remove the item before the cursor bc we operate the list with the 
    // cursor always pointing to the sentinal
    return remove_response_item(dlist,dlist->queue->prev);
}

/**
 * Puts a new request on the queue.  The queue takes ownership.
 * Ultimately the request callback is responsible for retiring/freeing 
 * the request.
 */
static void outstanding_push_head(struct request_queue* self,struct response *r) {
    // This lock gets reused here, but a list-specific lock could be used.
    // There won't be a lot of contention on it, so it should be fine to share.
    AcquireSRWLockExclusive(&self->lock);
    insert_response(self->outstanding.queue,r);
    ReleaseSRWLockExclusive(&self->lock);
}

/** 
 * @returns 0 when empty, otherwise the oldest request on the queue.
 */
static struct response* outstanding_pop_tail(struct request_queue* self) {
    AcquireSRWLockExclusive(&self->lock);
    struct response *item=remove_response(&self->outstanding);
    ReleaseSRWLockExclusive(&self->lock);
    return item;
}

/** 
 * Remove an response r from the outstanding responses list.
 * If r has already been removed, this will be a no-op.
 * 
 * @returns 0 when empty, otherwise the oldest request on the queue.
 */
static void outstanding_remove(struct request_queue* self,struct response *r) {
    AcquireSRWLockExclusive(&self->lock);
    remove_response_item(&self->outstanding,r);
    ReleaseSRWLockExclusive(&self->lock);
}

struct and_then_request {
    struct request request;
    struct request *cause;
    struct request *effect;
    enum reponse_status (*map)(struct request *effect,struct response *cause);
};

static enum response_status and_then_cb(struct request_queue *q,struct response *response,const struct request  *request);

/**
 * Example processing loop:
 * 
 * ```
 * // run till all outstanding events are consumed
 * while(request_queue_pump(&q));
 * // Check q.last_ecode for error state
 * ```
 * 
 * @returns 0 when processing is done, otherwise 1.
 */
static int request_queue_pump(struct request_queue* self) {
    if(self->last_ecode!=response_ok)         
        return 0; // Some request had a problem.  Abort.
    struct request *req=todo_pop_tail(self);
    if(!req || !req->callback) return 0;
    struct response *rsp=req->response;
    outstanding_push_head(self,rsp);
    // Note: Don't use the request after the callback.
    //       This way the callback can deallocate the request if desired.
    const enum response_status ecode=req->callback(self,req->response,req);
    
    // and_then requests are special.
    //
    // They are "owned" and need to be deallocated after use.
    // This is done here, since we can be sure the request is done and
    // we can still identify the request object.
    //
    // The and_then object can't deallocate itself since it might be the
    // cause for another and_then object.	
    if(req->callback==and_then_cb) { 
        struct and_then_request *atr=containerof(req,struct and_then_request,request);
        free(atr);
    }

    rsp->status=ecode;
    outstanding_remove(self,rsp);
    SetEvent(rsp->done);
    self->last_ecode=ecode;
    return 1;
}

static void request_submit(struct request_queue* self, struct request* req) {
    struct response *res=req->response;
    res->status=response_waiting;
    res->done=CreateEvent(0,TRUE,FALSE,0);
    todo_push_head(self,req);    
}

static enum response_status await(struct request_queue *q, struct response *r) {	
    static void request_queue_flush(struct request_queue *self);
    while(r->status==response_waiting)
        WaitForSingleObject(r->done,30);
    CloseHandle(r->done);
    r->done=INVALID_HANDLE_VALUE;
    return (q->last_ecode==response_err)?response_err:r->status;
}

/** 
 * Utility function for initializing the request struct.
 * Using this function ensures you won't forget to set a field somewhere.
 * @returns self
 */
static struct request* request_init(
    struct request* self,
    struct response *response,
    enum response_status (*callback)(struct request_queue*, struct response*,const struct request*))
{
    // A response is not waitable unless it's request is submitted.
    // "Initialization" of the response happens in request_submit().
    response->done=INVALID_HANDLE_VALUE;
    response->status=response_uninitialized;
    self->response=response;
    self->callback=callback;
    self->next=self->prev=self;	
    return self;
}

//
//                  AND_THEN
//					Combinator for sequencing events
//

#if 0 // here for reference.  Actually declared towards the top.
struct and_then_request {
    struct request request;
    struct request *cause;
    struct request *effect;
    enum reponse_status (*map)(struct request *effect,struct response *cause);
};
#endif

static enum response_status
and_then_cb(
    struct request_queue *q,
    struct response *response,
    const struct request  *request
){        
    DBG_(q->logger,"and_then_cb");
    struct and_then_request *src=containerof(request,struct and_then_request,request);
    DBG_(q->logger,"\tenqueue cause for %p",src);
    struct response* cr=src->cause->response;
    request_submit(q,src->cause);
    if(!(await(q,cr)==response_ok))
        goto Error;
    
    if(!(src->map(src->effect,cr)==response_ok))
        goto Error;
    DBG_(q->logger,"\tenqueue effect for %p",src);
    request_submit(q,src->effect);
    if(!(await(q,src->effect->response)==response_ok))
        goto Error;

    return response_ok;
Error:
    free(src);
    return response_err;
}

/**
 * Generate a new request. Does _not_ enqueue the request.
 * The request will wait for 'cause', and then submit request 'effect'.
 * 
 * The response object referenced by the and_then_request is effect's response.
 * 
 * b->response should hold a pointer to the (uninitialized) response object for b.
 * @return A request pointer belonging to the and_then_request.
 *         It can then be used to generate other and_then_requests or 
 *         be directly submitted to the queue.
 */
static struct request* 
and_then(
    struct request *cause,
    struct request *effect,
    enum reponse_status (*map)(struct request *effect,struct response *cause)
) {
    struct and_then_request *atr=
        (struct and_then_request*)malloc(sizeof(struct and_then_request));
    atr->cause=cause;
    atr->effect=effect;
    atr->map=map;
    request_init(&atr->request,effect->response,and_then_cb);
    return &atr->request;
}

//
//                  THREAD POOL
//

/**
 * The thread pool executes events from the queue, q.
 * The lifetime of q must exceed that of this object.
 */
struct thread_pool {
    volatile int running;
    struct request_queue *q;
    HANDLE thread;
};

static void pump(void *param) {
    struct thread_pool *tp=(struct thread_pool*) param;
    request_queue_pump(tp->q);
}

static void loop(void *param) {
    struct thread_pool *tp=(struct thread_pool*) param;
    tp->running=1;
    while(tp->running && tp->q->last_ecode!=response_err) {
        request_queue_wait_for_work(tp->q);    
        QueueUserWorkItem((LPTHREAD_START_ROUTINE)pump,tp,WT_EXECUTEDEFAULT);
    }
    tp->q->is_running=0;
    DBG_(tp->q->logger,"Event processing loop has exited.");
}

static void thread_pool_start(struct thread_pool *tp,struct request_queue *q) {
    tp->running=-1; // not started yet.
    tp->q=q;
    tp->thread=CreateThread(0,0,(LPTHREAD_START_ROUTINE)loop,tp,0,0);
}

static void thread_pool_sig_stop(struct thread_pool *tp) {	
    while(tp->running<0); // spin till started.
    tp->running=0;    
    SetEvent(tp->q->work_available);
}

static void thread_pool_wait(struct thread_pool *tp) {
    WaitForSingleObject(tp->thread,INFINITE);
    CloseHandle(tp->thread);
    tp->thread=INVALID_HANDLE_VALUE;
}