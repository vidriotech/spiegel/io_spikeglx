// Copyright (c)  Vidrio Technologies, All Rights Reserved
// Author: Nathan Clack <nathan@vidriotech.com>

#include <windows.h>

static const char* env(const char* var) {
	// Windows limits environment variable contents to 32k.
	char buf1[4096]={0},buf2[4096]={0};
	if(!GetEnvironmentVariableA(var,buf1,sizeof(buf1))) {
		// Didn't find the variable, look for a dot env file

		// Search for the .env file.  Walk up the directory tree starting from
		// the current directory until one is found.
		HANDLE h=INVALID_HANDLE_VALUE;
		char path[1024];
		char *end=path+GetCurrentDirectoryA(sizeof(path),path);
		while(h==INVALID_HANDLE_VALUE && path[0]) {
			char filename[1024]={0};
			strcat_s(filename,sizeof(filename),path);
			strcat_s(filename,sizeof(filename),"\\.env");
			h=CreateFileA(filename,FILE_READ_ACCESS,FILE_SHARE_READ,0,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0);
			char* p=end;
			while(--p>path && *p!='\\');
			*p='\0';
			end=p;
		}
		// Read contents and close the file.
		{   
			DWORD nread;			
			if(h==INVALID_HANDLE_VALUE) goto DotEnvNotFound;
			if(!ReadFile(h,buf1,sizeof(buf1),&nread,0)) {
				CloseHandle(h);
				goto DotEnvNotRead;
			}			
			CloseHandle(h);
		}
		// The contents are assumed to be lines (\n terminated) that have the following format:
		//
		//		line ::= variable_name '=' variable_contents line_terminator line
	    //		       | variable_name '=' variable_contents '\0'
		//             | '#' variable_contents line_terminator line
		//      line_terminator    ::= '\r\n' | '\n'
		//      variable_name      ::= [^=\r\n\0\t ]
		//      variable_contents  ::= [^=\r\n\0]
		struct { char *beg,*end;} name={0},value={0}; // end points to one past the last character		
		for(char *c=buf1;(c<buf1+sizeof(buf1)) && !(*c==0 && value.beg==0);++c) {
			// Check for end of value
			if(value.beg) {
				switch(*c) {
					case '#':  
						value.end=c; 
						while( (c<buf1+sizeof(buf1)) && !(*c==0||*c=='\n')) ++c; // advance to eol
						break;
					// adjust in case /r/n line endings are used
					case '\n': value.end=c-(c[-1]=='\r'); break;						
					case ' ':						
					case '\t':
					case '\0': value.end=c; break;
					case '=': goto DotEnvParseError;
					default:;
				}

				// 1. add name,value to system environment variables
				if(value.end) {
					// check non-empty
					if(name.beg==name.end || value.beg==value.end)
						goto DotEnvParseError;
					*name.end=0;
					*value.end=0;
					if(!SetEnvironmentVariableA(name.beg,value.beg))
						goto EnvSetError;
					// 2. set up for next name,value pair
					value.beg=value.end=0;
					name.beg=name.end=0;
				}

			} else if(name.beg) { // Check for end of name
				switch(*c) {
					case '#':
					case '\r':
					case '\n':
					case '\0':						
					case ' ':
					case '\t':
						goto DotEnvParseError;
					case '=':
						name.end=c;
						value.beg=c+1;
						break;
						break;
					default:;
				}
			} else {
				// Normal line processing (not in value or name)
				switch(*c) {
					case '#': // comment.
						while( (c<buf1+sizeof(buf1)) && !(*c==0||*c=='\n')) ++c; // advance to eol
						break;
					case '\r':
					case '\n':
					case '\0': 
					case ' ':
					case '\t':
						break;
					case '=':
						goto DotEnvParseError;
					default:
						if(!name.beg)
							name.beg=c;
				}
			}
		}
		// try again
		if(!GetEnvironmentVariableA(var,buf1,sizeof(buf1)))
			return 0;
	}
	{
		// do substitutions
		int nlast=0,n=0;
		char *src=buf1,*dst=buf2;
		do {
			nlast=n;
			if(!(n=ExpandEnvironmentStringsA(src,dst,sizeof(buf1))))
				return 0;
			{char* t=src; src=dst; dst=t;} // swap
		} while(n!=nlast);
		return _strdup(src);
	}
	
EnvSetError:
DotEnvParseError:
DotEnvNotRead:
DotEnvNotFound:
	return 0;
}