// Copyright (c)  Vidrio Technologies, All Rights Reserved
// Author: Nathan Clack <nathan@vidriotech.com>

#define _CRT_SECURE_NO_WARNINGS

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

#include "../src/spikegl.h"
#include "dotenv.c"


#define DBG(...) LOG(__VA_ARGS__)
#define LOG(...) logger(NULL,0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define ERR(...) logger(NULL,1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define CHECK(ecode) do{if(ecode) {ERR("Error: %s\n",#ecode); goto Error;}}while(0)
#define ASRT(e) do{if(!(e)) {ERR("Error: %s\n",#e); goto Error;}}while(0)

static int mem_leak_controls();


//
//                  MEMORY LEAK DETECTION
//

#define _CRT_SECURE_NO_WARNINGS
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>

#define REQUIRE_REPORT_ALLOCATION_SIZE struct { _CrtMemState beg,end,delta; } ras_6546__={0}

#define REPORT_ALLOCATION_SIZE(...) \
    _CrtMemCheckpoint(&ras_6546__.beg); \
    __VA_ARGS__ \
    _CrtMemCheckpoint(&ras_6546__.end); \
    _CrtMemDifference(&ras_6546__.delta,&ras_6546__.beg,&ras_6546__.end); \
    LOG("\tAllocation %f kB",ras_6546__.delta.lTotalCount/1024.0f);

//
//                  LOGGER
//

static void logger(const struct spikegl_logger *self,int is_error,const char *file,int line,const char* function,const char *fmt,...) {
    char buf1[1024]={0},buf2[1024]={0};
    va_list ap;
    va_start(ap,fmt);
    vsprintf(buf1,fmt,ap);
    va_end(ap);
#if 1
    sprintf(buf2,"%s(%d): %s()\n\t - %s\n",file,line,function,buf1);
#else
    sprintf(buf2,"%s\n",buf1);
#endif
    puts(buf2);
}

//
//                  MAIN
//

int main(int argc,char* argv[]) {
    struct spikegl_reader_ctx ctx={        
        .workspace=malloc(1<<15),
        .bytesof_workspace=1<<15,
        .logger.logger=logger
    };

    mem_leak_controls();

    // make request    
    struct spikegl_future f;
    {
        struct spikegl_reader r;
        spikegl_reader_create(&r,&ctx);
        #if 1 // bigish roi, hits 4 files
            struct spikegl_roi roi={
                .offset={.channels=64, .time=44556646+10000},
                .shape ={.channels=64, .time=1000000}
            };
        #else // small roi, hits one file
            struct spikegl_roi roi={
                .offset={.channels=64, .time=44611930},
                .shape ={.channels=64, .time=100}
            };
        #endif 
        const size_t nbytes=spikegl_reader_nbytes(&roi);
        ASRT(nbytes==(64*roi.shape.time*2));
        uint16_t *buf;
        DBG("Allocating %llu bytes for roi.",(uint64_t)nbytes);
        ASRT(buf=(uint16_t*)malloc(nbytes));
        CHECK(spikegl_reader_open(&r,env("SPIEGEL_IO_TEST_DATA"),env("SPIEGEL_IO_TEST_DATA_STREAM")));
        CHECK(spikegl_reader_read_async(&f,&r,&roi,buf,nbytes));
        spikegl_reader_destroy(&r);
    }

    // resolve request
    {
        struct spikegl_roi roi;
        void *buf=0;
        CHECK(spikegl_await(&f,&roi,&buf,0));        
        free(buf);
    }

    free(ctx.workspace);
    
    if(_CrtDumpMemoryLeaks())
        return __LINE__; 
    return 0;
Error:
    return 1;
}

/** 
 * This runs some controls just to make sure everything is right before 
 * actually looking for leaks.  
 * 
 * Also does some setup.
 */
static int mem_leak_controls() {
	// Send all reports to STDOUT
    _CrtSetReportMode(_CRT_WARN,_CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_WARN,_CRTDBG_FILE_STDOUT);
    _CrtSetReportMode(_CRT_ERROR,_CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_ERROR,_CRTDBG_FILE_STDOUT);
    _CrtSetReportMode(_CRT_ASSERT,_CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_ASSERT,_CRTDBG_FILE_STDOUT);
    REQUIRE_REPORT_ALLOCATION_SIZE;

    #ifndef _DEBUG
    LOG("WARNING: This only works when compiled with a Debug configuration.");
    #endif

    LOG("Control: Starting with no leaks.");
    if(_CrtDumpMemoryLeaks())
        return __LINE__;

    LOG("Control:");	
    REPORT_ALLOCATION_SIZE(
        float* ctrl=(float*)malloc(4096);
    );
    LOG("=>\tAttempt to detect expected leak.");
    if(_CrtDumpMemoryLeaks())
        LOG("\n=>\tFound expected leak.");
    else {
        LOG("FAILED to detect expected leak.  This was probably not compiled with the required debug flags.");
        return __LINE__;
    }
    free(ctrl);		
    if(_CrtDumpMemoryLeaks())
        return __LINE__; 
	return 0;
}