// Copyight (c)  Vidrio Technologies, All Rights Reserved
// Author: Nathan Clack <nathan@vidriotech.com>

#define  _CRT_SECURE_NO_WARNINGS

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

#include "../src/spikegl.h"
#include "dotenv.c"

#define LOG(...) logger(NULL,0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define ERR(...) logger(NULL,1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define CHECK(ecode) do{if(ecode) {ERR("Error: %s\n",#ecode); goto Error;}}while(0)

static void logger(const struct spikegl_logger *self,int is_error,const char *file,int line,const char* function,const char *fmt,...) {
    char buf1[1024]={0},buf2[1024]={0};
    va_list ap;
    va_start(ap,fmt);
    vsprintf(buf1,fmt,ap);
    va_end(ap);
#if 1
    sprintf(buf2,"%s(%d): %s()\n\t - %s\n",file,line,function,buf1);
#else
    sprintf(buf2,"%s\n",buf1);
#endif
    puts(buf2);
	OutputDebugStringA(buf2);
}

int main(int argc,char* argv[]) {
    struct spikegl_reader r;
    struct spikegl_reader_ctx ctx={
        .workspace=malloc(1<<14),
        .bytesof_workspace=1<<14,
        .logger.logger=logger
    };
    CHECK(spikegl_reader_create(&r,&ctx));
    CHECK(spikegl_reader_open(&r,env("SPIEGEL_IO_TEST_DATA"),env("SPIEGEL_IO_TEST_DATA_STREAM")));
    spikegl_reader_destroy(&r);    
    return 0;
Error:
    return 1;
}