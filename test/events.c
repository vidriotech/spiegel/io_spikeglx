// Copyright (c) 2017 Vidrio Technologies, All Rights Reserved
// Author: Nathan Clack <nathan@vidriotech.com>

/**
 * @file Test out the event system
 */

//
//                  MEMORY LEAK DETECTION
//

#define _CRT_SECURE_NO_WARNINGS
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>

#define REQUIRE_REPORT_ALLOCATION_SIZE struct { _CrtMemState beg,end,delta; } ras_6546__={0}

#define REPORT_ALLOCATION_SIZE(...) \
    _CrtMemCheckpoint(&ras_6546__.beg); \
    __VA_ARGS__ \
    _CrtMemCheckpoint(&ras_6546__.end); \
    _CrtMemDifference(&ras_6546__.delta,&ras_6546__.beg,&ras_6546__.end); \
    LOG("\tAllocation %f kB",ras_6546__.delta.lTotalCount/1024.0f);

//
//                  PRELUDE
//

#include <windows.h>
#include <stddef.h>

#include <stdarg.h>
#include <stdio.h>

#define DBG_(...)
#define DBG(...)
#define LOG(...) logger(0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define ERR(...) logger(1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define CHECK(e) do{if(!(e)) {ERR("Error: Assertion failed -- %s\n",#e); goto Error;}}while(0)

#define containerof(P,T,M) ((T*)((char*)(P)-offsetof(T,M)))

typedef void (*logger_t)(int is_error,const char *file,int line,const char* function,const char *fmt,...);


static int waiting_logger_count=0;
static void logger(int is_error,const char *file,int line,const char* function,const char *fmt,...) {
	++waiting_logger_count;
    char buf1[1024]={0},buf2[1024]={0};
    va_list ap;
    va_start(ap,fmt);
    vsprintf_s(buf1,sizeof(buf1),fmt,ap);
    va_end(ap);
#if 1
    sprintf_s(buf2,sizeof(buf2),"%s(%d): %s()\n\t - %s\n",file,line,function,buf1);
#else
    sprintf_s(buf2,sizeof(buf2),"%s\n",buf1);
#endif
    puts(buf2);
	OutputDebugStringA(buf2);
	--waiting_logger_count;
}

#include "../src/events.c"

static enum response_status open_file_cb(struct request_queue *,struct response*,const struct request*);
static enum response_status read_file_cb(struct request_queue *,struct response*,const struct request*);
static enum response_status process_data_cb(struct request_queue *,struct response*,const struct request*);

static enum reponse_status map_fopen_to_fread(struct request *effect,struct response *cause);
static enum reponse_status map_fread_to_process_data(struct request *effect,struct response *cause);

#include <stdint.h>

struct open_file_req {struct request request; char path[MAX_PATH];};
struct open_file_res {struct response response;HANDLE h;};
struct read_req{struct request request; HANDLE h;};
struct read_res{struct response response; char *buf; size_t nbytes;};
struct process_data_req{struct request request; char *buf; size_t nbytes;};
struct process_data_res{struct response response; uint64_t hash;};

enum reponse_status example(struct request_queue *q) {
	struct open_file_req fopen_req={.path=__FILE__};
	struct open_file_res fopen_res;
	struct read_req fread_req;
	struct read_res fread_res;		
	struct process_data_req process_data_req;
	struct process_data_res process_data_res={0};	
	REQUIRE_REPORT_ALLOCATION_SIZE;

	request_init(&fopen_req.request,&fopen_res.response,open_file_cb);
	request_init(&fread_req.request,&fread_res.response,read_file_cb);
	request_init(&process_data_req.request,&process_data_res.response,process_data_cb);

	// Memory for and_then requests is managed internally.
	// They're mostly opaque to the caller.
	//
	// The problem is the caller might want to grab the response from a1
	// after the await...would need to document that a1 might get freed.
	REPORT_ALLOCATION_SIZE(
		struct request *a0=and_then(&fopen_req.request,&fread_req.request,map_fopen_to_fread);
	);
	REPORT_ALLOCATION_SIZE(
		struct request *a1=and_then(a0,&process_data_req.request,map_fread_to_process_data);
	);
	request_submit(q,a1);
	enum response_status ecode=await(q,a1->response);
	LOG("HASH: 0x%08x",process_data_res.hash);
	return ecode;
}


static enum reponse_status 
map_fopen_to_fread(
	struct request *effect,
	struct response *cause
) {
	DBG("map_fopen_to_fread");
	struct open_file_res *res=containerof(cause,struct open_file_res,response);
	struct read_req *req=containerof(effect,struct read_req,request);
	req->h=res->h;
	return response_ok;
}

static enum reponse_status 
map_fread_to_process_data(
	struct request *effect,
	struct response *cause
) {
	DBG("map_fread_to_process_data");
	struct read_res *res=containerof(cause,struct read_res,response);
	struct process_data_req *req=containerof(effect,struct process_data_req,request);
	req->buf=res->buf;
	req->nbytes=res->nbytes;
	return response_ok;
}

static enum response_status
open_file_cb(
	struct request_queue *q,
	struct response* response,
	const struct request* request
) {
	DBG("open_file_cb");
	struct open_file_req *req=containerof(request,struct open_file_req,request);
	struct open_file_res *res=containerof(response,struct open_file_res,response);
	DBG("%s",req->path);
	res->h=CreateFile(req->path,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,FILE_FLAG_OVERLAPPED,0);
	CHECK(res->h!=INVALID_HANDLE_VALUE);
	return response_ok;
	Error:
	ERR("Failed to open %s",req->path);
 	return response_err;
}

static enum response_status
read_file_cb(
	struct request_queue *q,
	struct response* response,
	const struct request* request
) {
	struct read_req *req=containerof(request,struct read_req,request);
	struct read_res *res=containerof(response,struct read_res,response);
	DBG("read_file_cb");
	{	// Allocate memory
		LARGE_INTEGER sz;
		sz.LowPart=GetFileSize(req->h,&sz.HighPart);
		res->nbytes=sz.QuadPart;
		CHECK(res->buf=malloc(sz.QuadPart));
	}
	{	// Read
		uint64_t n=res->nbytes;
		OVERLAPPED o={.hEvent=CreateEvent(0,TRUE,FALSE,0)};
		uint64_t offset=0;
		uint8_t *buf=res->buf;
		while(n) {
			DWORD nn=(DWORD)n,nread;
			o.Pointer=(void*)offset;
			ReadFile(req->h,buf,nn,0,&o);
			GetOverlappedResult(req->h,&o,&nread,TRUE);
			offset+=nn;
			buf+=nn;
			n-=nn;
		}
	}
	
	return response_ok;
Error:
	return response_err;
}

static enum response_status
process_data_cb(
	struct request_queue *q,
	struct response* response,
	const struct request* request
) {
	struct process_data_req *req=containerof(request,struct process_data_req,request);
	struct process_data_res *res=containerof(response,struct process_data_res,response);	
	DBG("process_data_cb");
	enum response_status ecode=response_ok;
	uint32_t hash=0;
	const char * const end=req->buf+req->nbytes;
	for(char*c=req->buf;c<end;++c)
		hash=hash*65599+(int)*c; // sdbm
	res->hash=hash;
	free(req->buf);
	return response_ok;
}


static void wait_for_logging_to_finish(void) {
	// See Note 1, below.	
	while(waiting_logger_count>0)
		Sleep(1);
}

static int mem_leak_controls();

int main() {
	struct thread_pool tp;
	struct request_queue q;
	if(mem_leak_controls())
		return __LINE__;
	atexit(wait_for_logging_to_finish);

    struct {DWORD beg,end;} nhandles;	
    GetProcessHandleCount(GetCurrentProcess(),&nhandles.beg);
    LOG("Handle count (start): %d",nhandles.beg);
	
	thread_pool_start(&tp,&q);
	request_queue_init(&q,0);	
	{int n; GetProcessHandleCount(GetCurrentProcess(),&n);LOG("\thandles: %d",n);}
	CHECK(example(&q)==response_ok);
	{int n; GetProcessHandleCount(GetCurrentProcess(),&n);LOG("\thandles: %d",n);}	

	
	thread_pool_sig_stop(&tp);
	thread_pool_wait(&tp);
	request_queue_destroy(&q);

	GetProcessHandleCount(GetCurrentProcess(),&nhandles.end);
    LOG("Handle count (end): %d",nhandles.end);
	if((nhandles.end-nhandles.beg)>4) { // ??? Don't know how to get rid of these 4 or if they're important
		LOG("\tLeaked %d handles.",nhandles.end-nhandles.beg);
		goto Error;
	}
		
	if(_CrtDumpMemoryLeaks())
		return __LINE__;

	return 0;
Error:
	return 1;
}


/** 
 * This runs some controls just to make sure everything is right before 
 * actually looking for leaks.  
 * 
 * Also does some setup.
 */
static int mem_leak_controls() {
	// Send all reports to STDOUT
    _CrtSetReportMode(_CRT_WARN,_CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_WARN,_CRTDBG_FILE_STDOUT);
    _CrtSetReportMode(_CRT_ERROR,_CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_ERROR,_CRTDBG_FILE_STDOUT);
    _CrtSetReportMode(_CRT_ASSERT,_CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_ASSERT,_CRTDBG_FILE_STDOUT);
    REQUIRE_REPORT_ALLOCATION_SIZE;

    #ifndef _DEBUG
    LOG("WARNING: This only works when compiled with a Debug configuration.");
    #endif

    LOG("Control: Starting with no leaks.");
    if(_CrtDumpMemoryLeaks())
        return __LINE__;

    LOG("Control:");	
    REPORT_ALLOCATION_SIZE(
        float* ctrl=(float*)malloc(4096);
    );
    LOG("=>\tAttempt to detect expected leak.");
    if(_CrtDumpMemoryLeaks())
        LOG("\n=>\tFound expected leak.");
    else {
        LOG("FAILED to detect expected leak.  This was probably not compiled with the required debug flags.");
        return 0;
    }
    free(ctrl);		
    if(_CrtDumpMemoryLeaks())
        return __LINE__; 
	return 0;
}

/* NOTES

(1)

I ran into a weird issue where one of the printfs (usually a vsnprintf_s) would
segfault after the main thread exited.  Inside of the vsnprintf_s, it was 
calling _free_dbg which relies on a global CriticalSection associated with the 
c runtime.  Once the c runtime cleans itself up after the main thread exits, 
that CriticalSection is gone, and there's a segfault.

Having a problem with the c runtime suddenly drop off the face of the earth 
while other threads are running seems like a problem.  Maybe that's why they 
sugget using the beginthread_ api when using the c runtime. For now this is
solved, by just keeping track of the logger messages waiting at exit. This is 
also nice because I can ensure everything gets logged (as opposed to truncating 
the message output when the main thread exits).
*/