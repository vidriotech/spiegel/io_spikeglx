// Copyight (c)  Vidrio Technologies, All Rights Reserved
// Author: Nathan Clack <nathan@vidriotech.com>

#define  _CRT_SECURE_NO_WARNINGS

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

#include "../src/spikegl.h"
#include "dotenv.c"

#define LOG(...) logger(NULL,0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define ERR(...) logger(NULL,1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define ECODE(ecode) do{if(ecode) {ERR("Error: %s\n",#ecode); goto Error;}}while(0)
#define CHECK(e)     do{if(!(e)) {ERR("Error: %s\n",#e); goto Error;}}while(0)

static void logger(const struct spikegl_logger *self,int is_error,const char *file,int line,const char* function,const char *fmt,...) {
    char buf1[1024]={0},buf2[1024]={0};
    va_list ap;
    va_start(ap,fmt);
    vsprintf(buf1,fmt,ap);
    va_end(ap);
#if 1
    sprintf(buf2,"%s(%d): %s()\n\t - %s\n",file,line,function,buf1);
#else
    sprintf(buf2,"%s\n",buf1);
#endif
    puts(buf2);
	OutputDebugStringA(buf2);
}

int main(int argc,char* argv[]) {
    struct spikegl_reader r;
    struct spikegl_reader_ctx ctx={
        .workspace=malloc(1<<15),
        .bytesof_workspace=1<<15,
        .logger.logger=logger
    };
    struct spikegl_roi roi;

    DWORD nhandles;
    GetProcessHandleCount(GetCurrentProcess(),&nhandles);
    LOG("nhandles: %d",nhandles);

    ECODE(spikegl_reader_create(&r,&ctx));
    ECODE(spikegl_reader_open(&r,env("SPIEGEL_IO_TEST_DATA"),env("SPIEGEL_IO_TEST_DATA_STREAM")));
    int ntrials=spikegl_reader_ntrials(&r);
    CHECK(ntrials==540);
    CHECK(spikegl_reader_get_roi(&r,&roi,30000)==spikegl_status_err);
    ECODE(spikegl_reader_get_roi(&r,&roi,8));
    CHECK(roi.offset.channels==0);
	CHECK(roi.offset.time=45481064);
    CHECK(roi.shape.channels==256);
	CHECK(roi.shape.time==197501);
	//CHECK(shape.time==51141305);
    //CHECK(shape.time==197178993); // ~2.2 hrs @ 25 kHz
    spikegl_reader_destroy(&r);
    LOG("Trial: starts at sample: %lld",roi.offset.time);
    LOG("Trial: %lld x %lld channels x time",roi.shape.channels,roi.shape.time);
	LOG("OK");

    GetProcessHandleCount(GetCurrentProcess(),&nhandles);
    LOG("nhandles: %d",nhandles);
    return 0;
Error:
	LOG("FAIL");
    return 1;
}