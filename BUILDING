Getting started
===============

Download the code from the repository

    git clone https://gitlab.com/vidriotech/spiegel/io_spikegl.git

To configure the build:

Method 1 - The build script
---------------------------

Use the supplied powershell script for configuring and building:

    cd io_spikegl
    ./scripts/build.ps1

This generates binaries in a build directory.  The first few lines of the
build script should be edited to control the build configuration (debug, 
release, etc), and the destination directory.  It's pretty likely you'll 
have to fix up some of the system include and library paths.

This doesn't generate a visual studio solution.  You can debug/profile the
executables in visual studio by using File > Add Project and selecting an 
executable.

This also doesn't package things up or add source code provenance data[4] to 
the build products.  The CMake version will generate an API that can tell
you where it came from which is useful for debug/support later on.

Method 2 - CMake
----------------------------

Invoke CMake to generate a Visual Studio solution and go from there.

    cd io_spikegl
    mkdir build
    cd build
    cmake ..

By default, on windows this will create a Visual Studio Solution `io_spikegl.sln`
in the `build` folder.  CMake[1] can also generate files for other build systems
including Makefiles, XCode projects, etc.  

To open the solution from the windows command line:

    start io_spikegl.sln

Customizing the configuration
-----------------------------

You may want to change one of the default values used during configuration.
This is done by modifying one of the many variables CMake tracks.  It's 
probably simplest to use

   cmake-gui ..

from the build directory to view and edit configuration variables.  Refer to the
cmake documentation for details.

Build Dependencies
==================

These are required to build the software.  Note: These are distinct from the 
dependencies required to actually run the built/packaged software.

The versions used/tested are indicated below.  It's possible other versions
could be used (older or newer) with small modifications.

* Windows 10 x64     - Operating system
* CMake 3.10         - Used to configure the build on different environments.
* Visual Studio      - Used to build the software.
* Git 2.7            - (Optional) Used to access the source code repository
                       and to track version information.  In particular,
                       when packing the software, a git tag and hash are used
                       so the version of the packaged software can later be 
                       tied to a specific revision of the source code [4].

Runtime Dependencies
====================

To use the built libraries and executables:

* Windows 10 x64     - Operating system

Build Targets
=============

INSTALL - Copies the built software into it's final location.  See 
          CMAKE_INSTALL_PREFIX.
PACKAGE - Creates a redistributable package containing mirroring the installed
          product.

[1]: https://cmake.org/
[2]: https://cmake.org/cmake/help/latest/variable/CMAKE_INSTALL_PREFIX.html
[3]: https://cmake.org/cmake/help/latest/module/FindCUDA.html?highlight=cuda_toolkit_root_dir
[4]: like the most recent git tag and commit hash.  The CMake build defines
      GIT_TAG and GIT_HASH for all targets.