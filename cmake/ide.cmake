# Copyright (c) 2017 Howard Hughes Medical Institute, All Rights Reserved
# Author: Nathan Clack <nathan@vidriotech.com>
include(CMakeToolsHelpers OPTIONAL) # vscode integration
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

