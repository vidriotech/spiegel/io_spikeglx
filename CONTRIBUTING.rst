==================
Contributing Guide
==================

Herein lies scraps of information intended to guide the hapless developer
towards a better understanding of how to work with this code.

Tips
----

Windows filesystem caching is amazing, but sometimes you really want to
measure what the actual effective disk bandwidth is.  Use RAMMap_.  Also 
see `this stackoverflow thread<https://stackoverflow.com/questions/478340/clear-file-cache-to-repeat-performance-testing>`_.



.. _RAMMap: http://technet.microsoft.com/en-us/sysinternals/ff700229.aspx

